package com.aspects.controller;

import com.aspects.dto.PrivilegeDto;
import com.aspects.service.PrivilegeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/privileges")
public class PrivilegesController {

    private final PrivilegeService privilegeService;

    @Autowired
    public PrivilegesController(PrivilegeService privilegeService) {
        this.privilegeService = privilegeService;
    }

    @GetMapping
    public @ResponseBody ResponseEntity<List<PrivilegeDto>> getAllPrivileges() {
        return ResponseEntity.ok(privilegeService.findAll());
    }
}


