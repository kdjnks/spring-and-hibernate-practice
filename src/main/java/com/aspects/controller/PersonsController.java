package com.aspects.controller;

import com.aspects.annotations.Security;
import com.aspects.dto.PersonDto;
import com.aspects.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/persons")
public class PersonsController {

    private final PersonService personService;

    @Autowired
    public PersonsController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping
    @Security(role = "admin")
    public @ResponseBody ResponseEntity<List<PersonDto>> getAllPersons(@RequestHeader String role) {
        return ResponseEntity.ok(personService.findAll());
    }

    @GetMapping("/{name}")
    @Security(role = "admin")
    public @ResponseBody ResponseEntity<List<PersonDto>> getAllPersons(@RequestHeader String role,
                                                                 @PathVariable String name) {
        return ResponseEntity.ok(personService.findPersonByName(name));
    }
}


