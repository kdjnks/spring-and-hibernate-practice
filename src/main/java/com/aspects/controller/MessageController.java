package com.aspects.controller;

import com.aspects.domain.test.Message;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Objects;

@Controller
@RequestMapping(value = "/scopes")
public class MessageController {

    @Resource(name = "messageRequestScope")
    private Message messageRequest;

    @Resource(name = "messageSessionScope")
    private Message messageSession;

    @GetMapping("/request")
    public @ResponseBody ResponseEntity<String> getRequestResult(@RequestParam String message) {
        messageRequest.setMessage(message);

        return ResponseEntity.ok(messageRequest.getMessage());
    }

    @GetMapping("/session")
    public @ResponseBody ResponseEntity<String> getSessionResult(@RequestParam String message) {
        if(Objects.isNull(messageSession.getMessage())) {
            messageSession.setMessage(message);
        }
        return ResponseEntity.ok(messageSession.getMessage() + " / " + message);
    }
}
