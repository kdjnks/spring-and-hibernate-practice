package com.aspects.dto;

import javax.validation.constraints.NotNull;

public class PersonDto {

    @NotNull
    private String name;

    private RoleDto role;

    public PersonDto(String name, RoleDto role) {
        this.name = name;
        this.role = role;
    }

    @Override
    public String toString() {
        return "PersonDto{" +
                "name='" + name + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoleDto getRole() {
        return role;
    }

    public void setRole(RoleDto role) {
        this.role = role;
    }
}
