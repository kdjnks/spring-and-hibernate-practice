package com.aspects.dto;

import java.util.List;

public class PrivilegeDto {

    private String name;

    private List<Long> dependsOn;

    public PrivilegeDto(String name, List<Long> dependsOn) {
        this.name = name;
        this.dependsOn = dependsOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getDependsOn() {
        return dependsOn;
    }

    public void setDependsOn(List<Long> dependsOn) {
        this.dependsOn = dependsOn;
    }
}
