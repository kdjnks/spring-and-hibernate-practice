package com.aspects.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/*
* GenericDao interface that handles any T object and PK object as a Primary Key
* */
public interface Dao<T, PK extends Serializable> {

    void save(T newInstance);

    void update(T transientObject);

    void delete(T persistentObject);

    void deleteById(PK id);

    T findById(PK id);

    List<T> findAll();

    Optional<T> getById(PK id);

    Optional<List<T>> getAll();

    void flush();

    void refresh(T entity);

    T merge(T entity);

}
