package com.aspects.dao;

import com.aspects.domain.Person;

import java.util.List;

public interface PersonDao {

    List<Person> findPersonsByName(String name);
}
