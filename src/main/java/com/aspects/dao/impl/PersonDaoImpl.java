package com.aspects.dao.impl;

import com.aspects.dao.PersonDao;
import com.aspects.domain.Person;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional
public class PersonDaoImpl extends GenericDaoHibernateImpl<Person, Long> implements PersonDao {

    @Override
    public List<Person> findPersonsByName(String personName) {
        final TypedQuery<Person> query = getDefaultEntityManager()
                .createQuery("" +
                        "SELECT p FROM Person p " +
                        "JOIN fetch p.role " + //JOIN Fetch loads Role and prevents n+1 problem here
                        "where p.name = :name", Person.class);
        query.setParameter("name", personName);

        return query.getResultList();
    }
}
