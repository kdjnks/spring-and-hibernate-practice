package com.aspects.repository;

import com.aspects.domain.Person;
import com.aspects.domain.Privilege;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PrivilegeRepository extends CrudRepository<Privilege, Long> {

    Optional<Privilege> findPrivilegeByName(String name);

}
