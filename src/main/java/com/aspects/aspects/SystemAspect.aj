package com.aspects.aspects;

public aspect SystemAspect {

    pointcut SystemAspect(String s): call(public void java.io.PrintStream.println(java.lang.String))
                    && args(s)
                    && !within(SystemAspect);

    void around(String s): SystemAspect(s) {
                System.out.println("Hello from aop!");
                proceed(s);
            }
}
