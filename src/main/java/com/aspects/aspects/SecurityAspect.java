package com.aspects.aspects;

import com.aspects.annotations.Security;
import com.aspects.exceptions.ForbiddenException;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class SecurityAspect {

    @Before("@annotation(securityAnnotation) && args(role)")
    public void securityAnnotationBefore(Security securityAnnotation, String role) throws Throwable {

        if(!Arrays.asList(securityAnnotation.role()).contains(role)) {
            throw new ForbiddenException(String.format("User with a role %s doesn't have permissions!", role));
        }

    }

}
