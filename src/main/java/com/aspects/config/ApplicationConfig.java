package com.aspects.config;

import com.aspects.domain.test.Message;
import com.aspects.domain.test.Person;
import com.aspects.domain.test.PrototypeBean;
import com.aspects.domain.test.SingletonBean;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;

@Configuration
public class ApplicationConfig {

    /**
     * All scopes
     * */

    @Bean
    //By default it is already a singleton if it's manageable by spring container.
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Person personSingletonScope() {
        return new Person();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Person personPrototypeScope() {
        return new Person();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public Message messageRequestScope() {
        return new Message();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public Message messageSessionScope() {
        return new Message();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public Message messageApplicationScope() {
        return new Message();
    }

    @Bean
    @Scope(scopeName = "websocket", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public Message messageWebsocketScope() {
        return new Message();
    }

    /**
     * Scopes mixing (Injecting prototype into a singleton)
     * */

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
    public PrototypeBean prototypeBean() {
        return new PrototypeBean();
    }

    @Bean
    public SingletonBean singletonBean() {
        return new SingletonBean();
    }
}
