package com.aspects.service;

import com.aspects.dao.PersonDao;
import com.aspects.domain.Person;
import com.aspects.dto.PersonDto;
import com.aspects.dto.RoleDto;
import com.aspects.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PersonService {

    private final PersonRepository personRepository;
    private final PersonDao personDao;

    @Autowired
    public PersonService(PersonRepository personRepository, PersonDao personDao) {
        this.personRepository = personRepository;
        this.personDao = personDao;
    }

    @Transactional(readOnly = true)
    public List<PersonDto> findAll() {
        Iterable<Person> personsIterable = personRepository.findAll();

        return StreamSupport.stream(personsIterable.spliterator(), false)
                .map(p -> new PersonDto(p.getName(), new RoleDto(p.getRole().getName())))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<PersonDto> findPersonByName(String name) {
        /*Optional<Person> person = personRepository.findPersonByName(name);

        return person
                .map(value -> new PersonDto(value.getName(), new RoleDto(value.getRole().getName())))
                .orElse(null);*/

        List<Person> persons = personDao.findPersonsByName(name);

        return persons.stream()
                .map(person -> new PersonDto(person.getName(), new RoleDto(person.getRole().getName())))
                .collect(Collectors.toList());
    }
}
