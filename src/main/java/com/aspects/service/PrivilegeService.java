package com.aspects.service;

import com.aspects.domain.Privilege;
import com.aspects.dto.PrivilegeDto;
import com.aspects.repository.PrivilegeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PrivilegeService {

    private final PrivilegeRepository privilegeRepository;

    @Autowired
    public PrivilegeService(PrivilegeRepository privilegeRepository) {
        this.privilegeRepository = privilegeRepository;
    }

    @Transactional(readOnly = true)
    public List<PrivilegeDto> findAll() {
        Iterable<Privilege> privilegeIterable = privilegeRepository.findAll();

        return StreamSupport.stream(privilegeIterable.spliterator(), false)
                .map(p -> new PrivilegeDto(p.getName(), p.getDependsOn()))
                .collect(Collectors.toList());
    }
}
