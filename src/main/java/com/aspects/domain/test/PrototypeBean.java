package com.aspects.domain.test;

public class PrototypeBean {

    private String beanName;

    public PrototypeBean() {
    }

    public PrototypeBean(String beanName) {
        this.beanName = beanName;
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
}
