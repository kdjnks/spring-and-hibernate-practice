package com.aspects.domain.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SingletonBean {

    @Autowired
    private PrototypeBean prototypeBean;


    public SingletonBean() {

    }

    public PrototypeBean getPrototypeBean() {
        return prototypeBean;
    }

    //Solution #1
    /*@Lookup
    public PrototypeBean getPrototypeBean() {
        return null;
    }*/

    //Solution #2 ProxyMode
}
