package com.aspects.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PRIVILEGES")
public class Privilege {

    @Id
    private Long id;

    @Column(name = "name")
    private String name;

    @ElementCollection
    @CollectionTable(
            name = "PARENT_PRIVILEGES",
            joinColumns = @JoinColumn(name = "privilege_id")
    )
    @Column(name = "depends_on")
    private List<Long> dependsOn;

    public Privilege() {}

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getDependsOn() {
        return dependsOn;
    }

    public void setDependsOn(List<Long> dependsOn) {
        this.dependsOn = dependsOn;
    }
}
