DROP TABLE IF EXISTS ROLES;
DROP TABLE IF EXISTS PERSONS;
DROP TABLE IF EXISTS DEPARTMENTS;
DROP TABLE IF EXISTS PRIVILEGES;

CREATE TABLE ROLES (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
);

CREATE TABLE PRIVILEGES (
    id INT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
);

CREATE TABLE PARENT_PRIVILEGES (
    privilege_id INT NOT NULL,
    depends_on INT NOT NULL,
    foreign key (depends_on) references PRIVILEGES(id)
);

CREATE TABLE DEPARTMENTS (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
);

CREATE TABLE PERSONS (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    role_id INT NOT NULL,
    department_id INT NULL,
    foreign key (role_id) references ROLES(id),
    foreign key (department_id) references DEPARTMENTS(id)
);

INSERT INTO ROLES (name) VALUES ('User'), ('Admin');

INSERT INTO PERSONS (name, role_id) VALUES ('Bob Marley', 1), ('John Sol', 2);

INSERT INTO PRIVILEGES (id, name) VALUES (1, 'Manage Payments'), (2, 'Create Payments'), (3, 'View Payments'), (4, 'Update Payments'), (5, 'Delete Payments');
INSERT INTO PARENT_PRIVILEGES (privilege_id, depends_on) VALUES (2, 1), (3, 1), (4, 1), (5, 1);