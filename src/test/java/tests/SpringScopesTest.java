package tests;

import com.aspects.config.ApplicationConfig;
import com.aspects.domain.test.Person;
import com.aspects.domain.test.PrototypeBean;
import com.aspects.domain.test.SingletonBean;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class SpringScopesTest {

    @Test
    public void singletonScopeTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);

        Person personSingletonA = (Person) applicationContext.getBean("personSingletonScope");
        Person personSingletonB = (Person) applicationContext.getBean("personSingletonScope");

        personSingletonA.setName("Custom name");

        Assert.assertEquals(personSingletonA.getName(), personSingletonB.getName());

        ((AbstractApplicationContext) applicationContext).close();
    }

    @Test
    public void prototypeScopeTest() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);

        Person personPrototypeA = (Person) applicationContext.getBean("personPrototypeScope");
        Person personPrototypeB = (Person) applicationContext.getBean("personPrototypeScope");

        personPrototypeA.setName("Custom name");
        personPrototypeB.setName("Custom name2");

        Assert.assertNotSame(personPrototypeA.getName(), personPrototypeB.getName());

        ((AbstractApplicationContext) applicationContext).close();
    }

    @Test
    public void mixingScopes() {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);

        SingletonBean singletonBean = applicationContext.getBean(SingletonBean.class);
        PrototypeBean prototypeBean = singletonBean.getPrototypeBean();

        SingletonBean singletonBean2 = applicationContext.getBean(SingletonBean.class);
        PrototypeBean prototypeBean2 = singletonBean2.getPrototypeBean();

        //They are not the same
        Assert.assertTrue(prototypeBean != prototypeBean2);
    }

}
